// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package fresheye

type option func(*context)

// ContextSize option sets the size of a window within which the words will be compared to each other.
// With size = 1, each word will be compared to its left and right neighbor only.
func ContextSize(n int) option {
	return func(c *context) {
		c.contextSize = n
	}
}

// SensitivityThreshold option sets the threshold for badness so that word pairs with badness below the threshold won't be reported.
func SensitivityThreshold(n int) option {
	return func(c *context) {
		c.sensitivityThreshold = float64(n)
	}
}

// ExcludeProperNames option sets whether words starting with a capital letter and not at the beginning of a sentence should be reported.
func ExcludeProperNames(ex bool) option {
	return func(c *context) {
		c.excludeProperNames = ex
	}
}
