// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package fresheye_test

import (
	_ "embed"
	"testing"

	"gitlab.com/opennota/fresheye"
)

//go:embed testdata/kuprin.txt
var text string

func BenchmarkCheck(b *testing.B) {
	for i := 0; i < b.N; i++ {
		it := fresheye.NewSimpleWordIterator(text)
		fresheye.Check(it)
	}
}
