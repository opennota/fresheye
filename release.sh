#!/bin/bash -x

set -e
set -o pipefail

echo Releasing $CI_COMMIT_TAG

apt-get update
apt-get install -y --no-install-recommends bzip2 zip jq

# Build and upload binaries

upload_file() {
  if ! curl --fail --silent --show-error \
    --header "Private-Token: ${PRIVATE_TOKEN}" \
    --form "file=@$1" \
    ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads | \
  jq -r .markdown; then
    exit 1
  fi
}

PV="fesrv_${CI_COMMIT_TAG#v}"
sep="  "$'\n'

FESRV_PATH=cmd/fesrv/main.go

GOOS=linux GOARCH=amd64 go build -ldflags='-s -w' -o ${PV}_linux_amd64 $FESRV_PATH
bzip2 -9 ${PV}_linux_amd64
links=`upload_file ${PV}_linux_amd64.bz2`
links+=$sep

GOOS=windows GOARCH=amd64 go build -ldflags='-s -w' -o ${PV}_windows_amd64.exe $FESRV_PATH
zip -9 ${PV}_windows_amd64.{zip,exe}
links+=`upload_file ${PV}_windows_amd64.zip`
links+=$sep

GOOS=windows GOARCH=386 go build -ldflags='-s -w' -o ${PV}_windows_386.exe $FESRV_PATH
zip -9 ${PV}_windows_386.{zip,exe}
links+=`upload_file ${PV}_windows_386.zip`
links+=$sep

# Create a release

jq -n \
  --arg tag_name "${CI_COMMIT_TAG}" \
  --arg name "${CI_COMMIT_TAG}" \
  --arg description "${CI_COMMIT_MESSAGE}"$'\n\n'"$links" \
  '{$tag_name,$name,$description}' | \
curl --fail --silent --show-error  \
  --header "Content-Type: application/json" \
  --header "Private-Token: ${PRIVATE_TOKEN}" \
  --data @- \
  --request POST \
  ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases
