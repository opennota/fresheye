fresheye [![License](http://img.shields.io/:license-agpl3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/fresheye?status.svg)](http://godoc.org/gitlab.com/opennota/fresheye) [![Coverage report](https://gitlab.com/opennota/fresheye/badges/master/coverage.svg)](https://gitlab.com/opennota/fresheye/commits/master) [![Pipeline status](https://gitlab.com/opennota/fresheye/badges/master/pipeline.svg)](https://gitlab.com/opennota/fresheye/commits/master)
========

## Install

    go get -u gitlab.com/opennota/fresheye
    go install gitlab.com/opennota/fresheye/cmd/fesrv@latest

## Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`

## See also

You can find the original Fresh Eye program here: http://kirsanov.com/fresheye/
