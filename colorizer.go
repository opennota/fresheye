// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package fresheye

import (
	"fmt"
	"html"
	"image/color"
	"math"
	"sort"
	"strings"
)

// Colorizer is a way to assign colors to words given a slice of bad word pairs.
type Colorizer struct {
	threshold float64
	painted   map[WordDescriptor]mapValue
}

type mapValue struct {
	badness float64
	counter int
	color   color.RGBA
}

var highlightColors = []color.RGBA{
	{235, 20, 255, 255},
	{255, 130, 25, 255},
	{0, 255, 20, 255},
}

func colorForTint(level float64, colorCounter int) color.RGBA {
	c := highlightColors[colorCounter%len(highlightColors)]
	level += (1 - level) * 0.1
	r := uint8(math.Round(255 + (float64(c.R)-255)*level))
	g := uint8(math.Round(255 + (float64(c.G)-255)*level))
	b := uint8(math.Round(255 + (float64(c.B)-255)*level))
	return color.RGBA{r, g, b, 255}
}

// NewColorizer returns a Colorizer with a given sensitivity threshold.
func NewColorizer(threshold int) *Colorizer {
	return &Colorizer{
		threshold: float64(threshold),
	}
}

// Colorize assigns colors to words from pairs.
// To get the color assigned to a word descriptor d, call Color(d).
func (c *Colorizer) Colorize(pairs []BadPair) {
	painted := map[WordDescriptor]mapValue{}
	colorCounter := 0
	for _, p := range pairs {
		badness := p.Badness
		if badness > 2000 {
			badness = 2000
		}
		level := (badness - c.threshold) / (2000 - c.threshold)
		if v, ok := painted[p.First]; ok {
			colorCounter = v.counter
		}
		if v, ok := painted[p.Second]; ok {
			colorCounter = v.counter
		}

		color := colorForTint(level, colorCounter)

		if v, ok := painted[p.First]; !ok || v.badness < badness {
			painted[p.First] = mapValue{
				badness: badness,
				counter: colorCounter,
				color:   color,
			}
		}
		if v, ok := painted[p.Second]; !ok || v.badness < badness {
			painted[p.Second] = mapValue{
				badness: badness,
				counter: colorCounter,
				color:   color,
			}
		}

		colorCounter++
	}
	c.painted = painted
}

// Color returns the color assigned to the word descriptor d.
// You must call Colorize before calling Color.
func (c *Colorizer) Color(d WordDescriptor) color.RGBA {
	return c.painted[d].color
}

// ToColoredHTML returns text with bad word pairs colored using HTML tags.
func ToColoredHTML(text string,
	contextSize int,
	sensitivityThreshold int,
	excludeProperNames bool,
) string {
	it := NewSimpleWordIterator(text)
	bad := Check(it,
		ContextSize(contextSize),
		SensitivityThreshold(sensitivityThreshold),
		ExcludeProperNames(excludeProperNames),
	)
	colorizer := NewColorizer(sensitivityThreshold)
	colorizer.Colorize(bad)
	descriptors := WordDescriptors(bad)
	sort.Slice(descriptors, func(i, j int) bool {
		return descriptors[i].Start() < descriptors[j].Start()
	})
	i := 0
	var buf strings.Builder
	for _, d := range descriptors {
		c := colorizer.Color(d)
		from, to := d.Start(), d.End()
		buf.WriteString(html.EscapeString(text[i:from]))
		buf.WriteString(fmt.Sprintf(`<span style="background-color: #%02x%02x%02x">`, c.R, c.G, c.B))
		buf.WriteString(text[from:to])
		buf.WriteString("</span>")
		i = to
	}
	buf.WriteString(html.EscapeString(text[i:]))
	return strings.ReplaceAll(buf.String(), "\n", "<br>\n")
}
